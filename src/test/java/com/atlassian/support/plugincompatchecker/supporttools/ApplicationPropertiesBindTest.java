package com.atlassian.support.plugincompatchecker.supporttools;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import static com.atlassian.support.plugincompatchecker.supporttools.ApplicationProperties.parseApplicationProperties;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ApplicationPropertiesBindTest
{
    @Test
    public void confluence51() throws Exception
    {
        final ApplicationProperties applicationProperties = readFile("/examples/confluence_51/application.xml");

        assertThat(applicationProperties.getServerId(), is("BM02-ZQ9N-DU47-0DJA"));
        assertThat(applicationProperties.getProduct().getName(), is("Confluence"));
        assertThat(applicationProperties.getProduct().getVersion(), is("5.2-CDOG-2920"));
        assertThat(applicationProperties.getBuildNumber(), is(4307));
        assertThat(applicationProperties.getServerBaseUrl(), is("https://pug.jira.com/wiki"));

        assertThat(applicationProperties.getEnabledInstalledPlugins(), hasSize(276));
        final InstalledPlugin firstPlugin = applicationProperties.getEnabledInstalledPlugins().get(0);
        assertThat(firstPlugin.getPluginKey(), is("com.atlassian.activeobjects.activeobjects-plugin"));
        assertThat(firstPlugin.getName(), is("ActiveObjects Plugin - OSGi Bundle"));
        assertThat(firstPlugin.getVersion(), is("0.19.22"));
        assertThat(firstPlugin.getStatus(), is("ENABLED"));
    }

    @Test
    public void confluence50() throws Exception
    {
        final ApplicationProperties applicationProperties = readFile("/examples/confluence_50/application-properties_application (5).xml");

        assertThat(applicationProperties.getServerId(), is("unknown"));
        assertThat(applicationProperties.getProduct().getName(), is("Confluence"));
        assertThat(applicationProperties.getProduct().getVersion(), is("5.0.1"));
        assertThat(applicationProperties.getBuildNumber(), is(4107));
        assertThat(applicationProperties.getServerBaseUrl(), is("http://confluence.hq.unity3d.com"));

        assertThat(applicationProperties.getEnabledInstalledPlugins(), hasSize(215));
        final InstalledPlugin firstPlugin = applicationProperties.getEnabledInstalledPlugins().get(0);
        assertThat(firstPlugin.getPluginKey(), is("com.atlassian.activeobjects.activeobjects-plugin"));
        assertThat(firstPlugin.getName(), is("ActiveObjects Plugin - OSGi Bundle"));
        assertThat(firstPlugin.getVersion(), is("0.19.21"));
        assertThat(firstPlugin.getStatus(), is("ENABLED"));
    }

    private ApplicationProperties readFile(String location) throws JAXBException, IOException
    {
        return parseApplicationProperties(getClass().getResourceAsStream(location));
    }
}
