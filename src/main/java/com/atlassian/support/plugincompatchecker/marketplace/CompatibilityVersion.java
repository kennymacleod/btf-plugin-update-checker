package com.atlassian.support.plugincompatchecker.marketplace;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CompatibilityVersion
{
    @JsonProperty
    private String version;

    @JsonProperty
    private Integer buildNumber;

    @Override
    public String toString()
    {
        return "CompatibilityVersion{" +
                "buildNumber=" + buildNumber +
                ", version='" + version + '\'' +
                '}';
    }


    public String getVersion()
    {
        return version;
    }

    public Integer getBuildNumber()
    {
        return buildNumber;
    }
}
