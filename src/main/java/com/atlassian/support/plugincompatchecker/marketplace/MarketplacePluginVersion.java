package com.atlassian.support.plugincompatchecker.marketplace;

import com.atlassian.support.plugincompatchecker.report.Plugin;
import com.atlassian.support.plugincompatchecker.report.TargetApplication;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketplacePluginVersion
{
    @JsonProperty
    private List<ApplicationCompatibility> compatibilities;

    @JsonProperty
    private List<MarketplaceLink> links;

    @JsonProperty
    private Integer buildNumber;

    @JsonProperty
    private String version;

    public ApplicationCompatibility getCompatibilityForProduct(TargetApplication targetApplication)
    {
        for (ApplicationCompatibility compatibility : compatibilities)
        {
            if (compatibility.matchesProduct(targetApplication))
            {
                return compatibility;
            }
        }
        return null;
    }

    public boolean isSameVersion(Plugin installedPlugin)
    {
        return installedPlugin.getVersion() != null && installedPlugin.getVersion().equalsIgnoreCase(this.version);
    }

    public String getJarDownloadUrl()
    {
        return getLink("binary");
    }

    public String getHomepageUrl()
    {
        return getLink("homepage");
    }

    private String getLink(String rel)
    {
        for (MarketplaceLink link : links)
        {
            if (rel.equalsIgnoreCase(link.rel))
            {
                return link.href;
            }
        }
        return null;
    }

    @Override
    public String toString()
    {
        return "MarketplaceVersion{" +
                "buildNumber=" + buildNumber +
                ", compatibilities=" + compatibilities +
                ", version='" + version + '\'' +
                '}';
    }

    public List<ApplicationCompatibility> getCompatibilities()
    {
        return compatibilities;
    }

    public List<MarketplaceLink> getLinks()
    {
        return links;
    }

    public Integer getBuildNumber()
    {
        return buildNumber;
    }

    public String getVersion()
    {
        return version;
    }
}
