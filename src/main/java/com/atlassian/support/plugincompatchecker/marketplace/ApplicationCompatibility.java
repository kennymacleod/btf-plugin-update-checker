package com.atlassian.support.plugincompatchecker.marketplace;

import com.atlassian.support.plugincompatchecker.report.TargetApplication;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationCompatibility
{
    @JsonProperty
    private String applicationName;

    @JsonProperty
    private CompatibilityVersion min;

    @JsonProperty
    private CompatibilityVersion max;

    @Override
    public String toString()
    {
        return "Compatibility{" +
                "max=" + max +
                ", min=" + min +
                '}';
    }

    public boolean matchesProduct(TargetApplication targetApplication)
    {
        return targetApplication.getProductName().equalsIgnoreCase(applicationName);
    }

    public boolean isCompatible(TargetApplication targetApplication)
    {
        final int applicationBuildNumber = targetApplication.getBuildNumber();
        return matchesProduct(targetApplication) && applicationBuildNumber>= min
                .getBuildNumber() && applicationBuildNumber <= max.getBuildNumber();
    }


    public String getApplicationName()
    {
        return applicationName;
    }

    public CompatibilityVersion getMin()
    {
        return min;
    }

    public CompatibilityVersion getMax()
    {
        return max;
    }
}
