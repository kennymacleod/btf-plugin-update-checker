package com.atlassian.support.plugincompatchecker.marketplace;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketplaceLink
{
    @JsonProperty
    public String href;

    @JsonProperty
    public String rel;
}
