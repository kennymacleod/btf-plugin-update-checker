package com.atlassian.support.plugincompatchecker.marketplace;

import com.atlassian.support.plugincompatchecker.report.Plugin;
import com.atlassian.support.plugincompatchecker.report.TargetApplication;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketplacePlugin
{
    @JsonProperty
    public Versions versions;

    @JsonProperty
    public List<MarketplaceLink> links;

    @JsonProperty
    public String name;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Versions
    {
        @JsonProperty
        public List<MarketplacePluginVersion> versions;
    }

    public ApplicationCompatibility getApplicationCompatibility(Plugin installedPlugin, TargetApplication targetApplication)
    {
        for (MarketplacePluginVersion marketplaceVersion : versions.versions)
        {
            if (marketplaceVersion.getVersion().equals(installedPlugin.getVersion()))
            {
                return marketplaceVersion.getCompatibilityForProduct(targetApplication);
            }
        }
        return null;
    }

    public MarketplacePluginVersion getHighestCompatibleVersion(TargetApplication targetApplication)
    {
        MarketplacePluginVersion highestCompatibleVersion = null;
        for (MarketplacePluginVersion marketplaceVersion : versions.versions)
        {
            final ApplicationCompatibility applicationCompatibility = marketplaceVersion
                    .getCompatibilityForProduct(targetApplication);

            if (applicationCompatibility != null && applicationCompatibility.isCompatible(targetApplication))
            {
                if (highestCompatibleVersion == null || marketplaceVersion.getBuildNumber() > highestCompatibleVersion
                        .getBuildNumber())
                {
                    highestCompatibleVersion = marketplaceVersion;
                }
            }
        }
        return highestCompatibleVersion;
    }

    public boolean hasHomePageUrl()
    {
        return getHomepageUrl() != null;
    }

    public String getHomepageUrl()
    {
        return getLink("homepage");
    }

    private String getLink(String rel)
    {
        for (MarketplaceLink link : links)
        {
            if (rel.equalsIgnoreCase(link.rel))
            {
                return link.href;
            }
        }
        return null;
    }
}