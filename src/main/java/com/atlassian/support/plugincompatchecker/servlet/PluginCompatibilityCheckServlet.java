package com.atlassian.support.plugincompatchecker.servlet;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.support.plugincompatchecker.report.MarketplacePluginCompatibilityReportFactory;
import com.atlassian.support.plugincompatchecker.report.PluginCompatibilityReport;
import com.atlassian.support.plugincompatchecker.report.PluginCompatibilityReportFactory;
import com.atlassian.support.plugincompatchecker.report.PluginContainer;
import com.atlassian.support.plugincompatchecker.report.ServerInstance;
import com.atlassian.support.plugincompatchecker.report.TargetApplication;
import com.atlassian.support.plugincompatchecker.supporttools.ApplicationProperties;

import static com.google.common.collect.ImmutableMap.of;
import static java.util.Collections.emptyMap;


public class PluginCompatibilityCheckServlet extends HttpServlet
{
    private static final String TEMPLATES_MODULE_KEY = "com.atlassian.support.plugin-compatibility-checker:template-resources";
    private static final String INPUT_FORM_TEMPLATE_NAME = "BtfPluginCheck.Input.inputForm.soy";
    private static final String REPORT_TEMPLATE_NAME = "BtfPluginCheck.Report.report.soy";

    private final TemplateRenderer templateRenderer;
    private PluginCompatibilityReportFactory reportFactory;
    private ApplicationPropertiesFileUploadHandler fileUploadHandler;
    private ExecutorService marketplaceRequestExecutor;

    public PluginCompatibilityCheckServlet(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    @Override
    public void init() throws ServletException
    {
        // how many threads before we piss off the Marketplace people?
        marketplaceRequestExecutor = Executors.newFixedThreadPool(10);
        reportFactory = new MarketplacePluginCompatibilityReportFactory(marketplaceRequestExecutor);
        fileUploadHandler = new ApplicationPropertiesFileUploadHandler();
    }

    @Override
    public void destroy()
    {
        if (marketplaceRequestExecutor != null)
        {
            marketplaceRequestExecutor.shutdownNow();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        renderTemplate(resp, INPUT_FORM_TEMPLATE_NAME, emptyMap());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            final ApplicationProperties applicationProperties = fileUploadHandler.parseSupportToolsApplicationProperties(request);

            // Fow now we get all of our data from the ApplicationProperties object
            final TargetApplication targetApplication = applicationProperties;
            final ServerInstance serverInstance = applicationProperties;
            final PluginContainer pluginContainer = applicationProperties;

            final PluginCompatibilityReport report = reportFactory.getPluginCompatibilityReport(targetApplication, serverInstance, pluginContainer);

            renderTemplate(resp, REPORT_TEMPLATE_NAME, of("report", report));
        }
        catch (JAXBException ex)
        {
            renderTemplate(resp, INPUT_FORM_TEMPLATE_NAME, of("inputXmlError", decodeJaxbException(ex)));
        }
        catch (Exception ex)
        {
            throw new ServletException(ex);
        }
    }

    private static String decodeJaxbException(JAXBException ex)
    {
        return ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
    }

    private void renderTemplate(HttpServletResponse httpResponse, String templateName, Map data) throws IOException
    {
        httpResponse.setContentType("text/html");
        templateRenderer.renderTo(httpResponse.getWriter(), TEMPLATES_MODULE_KEY, templateName, data);
    }
}