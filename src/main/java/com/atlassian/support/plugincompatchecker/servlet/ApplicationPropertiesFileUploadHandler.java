package com.atlassian.support.plugincompatchecker.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;

import com.atlassian.support.plugincompatchecker.supporttools.ApplicationProperties;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class ApplicationPropertiesFileUploadHandler
{
    private static final String APPLICATION_PROPERTIES_FIELD = "applicationProperties";

    private ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());

    public ApplicationProperties parseSupportToolsApplicationProperties(final HttpServletRequest request) throws FileUploadException, JAXBException, IOException
    {
        final List<FileItem> items = upload.parseRequest(request);
        for (FileItem item : items)
        {
            if (APPLICATION_PROPERTIES_FIELD.equals(item.getFieldName()))
            {
                return parseApplicationProperties(item);
            }
        }
        throw new RuntimeException("No " + APPLICATION_PROPERTIES_FIELD + " file found in submission");
    }

    private ApplicationProperties parseApplicationProperties(FileItem file) throws JAXBException, IOException
    {
        try {
            return ApplicationProperties.parseApplicationProperties(file.getInputStream());
        }
        finally
        {
            file.delete();
        }
    }
}
