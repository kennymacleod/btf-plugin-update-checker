package com.atlassian.support.plugincompatchecker.supporttools;

public class SystemProperties extends GenericElement
{
    public int getBuildNumber()
    {
        return Integer.parseInt(getMatchingElementContent("build.number", "build-number"));
    }

    public String getServerBaseUrl()
    {
        return getMatchingElementContent("server.base.url", "server-base-url");
    }

}