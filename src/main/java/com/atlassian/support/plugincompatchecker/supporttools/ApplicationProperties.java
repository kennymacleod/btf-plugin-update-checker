package com.atlassian.support.plugincompatchecker.supporttools;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.support.plugincompatchecker.report.Plugin;
import com.atlassian.support.plugincompatchecker.report.PluginContainer;
import com.atlassian.support.plugincompatchecker.report.ServerInstance;
import com.atlassian.support.plugincompatchecker.report.TargetApplication;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement(name = "properties")
@XmlAccessorType(FIELD)
public class ApplicationProperties implements TargetApplication, ServerInstance, PluginContainer {
    @XmlElement(name = "product")
    private Product product;

    @XmlElement(name = "serverId")
    private String serverId;

    @XmlElement(name = "system-properties")
    private SystemProperties systemProperties;

    @XmlElementWrapper(name = "enabled-plugins")
    @XmlElement(name = "plugin")
    private List<InstalledPlugin> enabledInstalledPlugins;

    public static ApplicationProperties parseApplicationProperties(InputStream inputStream) throws JAXBException, IOException
    {
        JAXBContext jaxb = JAXBContext.newInstance(ApplicationProperties.class);
        return (ApplicationProperties) jaxb.createUnmarshaller().unmarshal(inputStream);
    }

    @Override
    public String getProductName()
    {
        return product.getName();
    }

    @Override
    public String getProductVersion()
    {
        return product.getVersion();
    }

    @Override
    public int getBuildNumber()
    {
        return systemProperties.getBuildNumber();
    }

    public Product getProduct()
    {
        return product;
    }

    public String getServerId()
    {
        return serverId;
    }

    @Override
    public String getServerBaseUrl()
    {
        return systemProperties.getServerBaseUrl();
    }

    List<InstalledPlugin> getEnabledInstalledPlugins()
    {
        return enabledInstalledPlugins;
    }

    @Override
    public Collection<? extends Plugin> getPlugins()
    {
        return enabledInstalledPlugins;
    }
}
