package com.atlassian.support.plugincompatchecker.supporttools;

import javax.xml.bind.annotation.XmlAttribute;

public class Product
{
    @XmlAttribute(name = "version")
    private String version;

    @XmlAttribute(name = "name")
    private String name;

    public String getVersion()
    {
        return version;
    }

    public String getName()
    {
        return name;
    }
}
