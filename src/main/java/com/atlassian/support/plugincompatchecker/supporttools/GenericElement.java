package com.atlassian.support.plugincompatchecker.supporttools;

import java.util.List;
import java.util.Map;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAnyElement;

import com.google.common.collect.ImmutableMap;

import org.w3c.dom.Element;

public abstract class GenericElement
{
    @XmlAnyElement
    private List<Element> data;

    private Map<String, String> elementValues;

    protected String getMatchingElementContent(String... elementNames)
    {
        for (String elementName : elementNames)
        {
            if (elementValues.containsKey(elementName))
            {
                return elementValues.get(elementName);
            }
        }
        return null;
    }

    // Called by JAXB runtime
    public void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
        final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        for (Element element : data)
        {
            builder.put(element.getTagName(), element.getTextContent());
        }
        elementValues = builder.build();
    }
}
