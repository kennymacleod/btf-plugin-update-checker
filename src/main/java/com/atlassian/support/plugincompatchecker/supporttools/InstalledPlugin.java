package com.atlassian.support.plugincompatchecker.supporttools;

import com.atlassian.support.plugincompatchecker.report.Plugin;

public class InstalledPlugin extends GenericElement implements Plugin {
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        InstalledPlugin that = (InstalledPlugin) o;

        if (getPluginKey() != null ? !getPluginKey().equals(that.getPluginKey()) : that.getPluginKey() != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return getPluginKey() != null ? getPluginKey().hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return "InstalledPlugin{" +
                "name='" + getName() + '\'' +
                ", pluginKey='" + getPluginKey() + '\'' +
                ", status='" + getStatus() + '\'' +
                ", version='" + getVersion() + '\'' +
                '}';
    }

    @Override
    public String getPluginKey()
    {
        return getMatchingElementContent("stp-properties-plugins-plugin-key", "key");
    }

    public String getStatus()
    {
        return getMatchingElementContent("stp-properties-plugins-plugin-status", "status");
    }

    @Override
    public String getVersion()
    {
        return getMatchingElementContent("stp-properties-plugins-plugin-version", "version");
    }

    @Override
    public String getName()
    {
        return getMatchingElementContent("stp-properties-plugins-plugin-name", "name");
    }
}
