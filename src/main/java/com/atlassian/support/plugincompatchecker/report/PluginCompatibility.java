package com.atlassian.support.plugincompatchecker.report;

import com.atlassian.support.plugincompatchecker.marketplace.ApplicationCompatibility;
import com.atlassian.support.plugincompatchecker.marketplace.MarketplacePlugin;
import com.atlassian.support.plugincompatchecker.marketplace.MarketplacePluginVersion;

public class PluginCompatibility
{
    private final Plugin installedPlugin;
    private final ApplicationCompatibility applicationCompatibility;
    private final MarketplacePluginVersion highestCompatibleVersion;
    private final MarketplacePlugin marketplacePlugin;

    public PluginCompatibility(Plugin installedPlugin, ApplicationCompatibility applicationCompatibility, MarketplacePluginVersion highestCompatibleVersion, MarketplacePlugin marketplacePlugin)
    {
        this.installedPlugin = installedPlugin;
        this.applicationCompatibility = applicationCompatibility;
        this.highestCompatibleVersion = highestCompatibleVersion;
        this.marketplacePlugin = marketplacePlugin;
    }

    public ApplicationCompatibility getApplicationCompatibility()
    {
        return applicationCompatibility;
    }

    public Plugin getInstalledPlugin()
    {
        return installedPlugin;
    }

    public MarketplacePluginVersion getHighestCompatibleVersion()
    {
        return highestCompatibleVersion;
    }

    public MarketplacePlugin getMarketplacePlugin()
    {
        return marketplacePlugin;
    }
}
