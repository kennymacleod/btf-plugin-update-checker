package com.atlassian.support.plugincompatchecker.report;

public interface TargetApplication
{
    String getProductName();
    String getProductVersion();
    int getBuildNumber();
}
