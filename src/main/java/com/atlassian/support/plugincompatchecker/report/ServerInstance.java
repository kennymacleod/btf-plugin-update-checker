package com.atlassian.support.plugincompatchecker.report;

public interface ServerInstance
{
    String getServerId();
    String getServerBaseUrl();
}
