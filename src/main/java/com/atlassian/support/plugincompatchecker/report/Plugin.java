package com.atlassian.support.plugincompatchecker.report;

public interface Plugin
{
    String getPluginKey();
    String getVersion();
    String getName();
}
