package com.atlassian.support.plugincompatchecker.report;

import com.atlassian.support.plugincompatchecker.marketplace.ApplicationCompatibility;
import com.atlassian.support.plugincompatchecker.marketplace.MarketplacePlugin;
import com.atlassian.support.plugincompatchecker.marketplace.MarketplacePluginVersion;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.filterValues;
import static com.google.common.collect.Maps.newLinkedHashMap;

public class PluginCompatibilityReport
{
    private final TargetApplication targetApplication;
    private final ServerInstance serverInstance;
    private final Map<Plugin, ApplicationCompatibility> pluginCompatibilities;
    private final Map<Plugin, MarketplacePlugin> plugins;
    private final Map<Plugin, MarketplacePluginVersion> highestCompatibleVersions;

    private PluginCompatibilityReport(TargetApplication targetApplication, ServerInstance serverInstance, Map<Plugin, MarketplacePlugin> plugins)
    {
        this.targetApplication = targetApplication;
        this.serverInstance = serverInstance;
        this.plugins = plugins;
        this.pluginCompatibilities = getCompatibilityMap(this.targetApplication);
        this.highestCompatibleVersions = getHighestCompatibleVersions();
    }

    public Map<Plugin, MarketplacePluginVersion> getHighestCompatibleVersions()
    {
        return getHighestCompatibleVersions(targetApplication);
    }

    public Map<Plugin, MarketplacePluginVersion> getHighestCompatibleVersions(TargetApplication targetApplication)
    {
        Map<Plugin, MarketplacePluginVersion> highestVersions = newLinkedHashMap();
        for (Plugin installedPlugin : plugins.keySet())
        {
            final MarketplacePlugin marketplacePlugin = plugins.get(installedPlugin);
            if (marketplacePlugin != null)
            {
                final MarketplacePluginVersion highestCompatibleVersion = marketplacePlugin
                        .getHighestCompatibleVersion(targetApplication);

                if (highestCompatibleVersion != null)
                {
                    highestVersions.put(installedPlugin, highestCompatibleVersion);
                }
            }
        }
        return highestVersions;
    }

    public List<PluginUpgradeSuggestion> getUpgradeSuggestions()
    {
        List<PluginUpgradeSuggestion> upgradeSuggestions = newArrayList();
        final Map<Plugin, MarketplacePluginVersion> highestCompatibleVersions = getHighestCompatibleVersions();

        for (Plugin installedPlugin : highestCompatibleVersions.keySet())
        {
            final MarketplacePluginVersion highestCompatibleVersion = highestCompatibleVersions.get(installedPlugin);
            if (highestCompatibleVersion != null && !highestCompatibleVersion.isSameVersion(installedPlugin))
            {
                upgradeSuggestions.add(new PluginUpgradeSuggestion(installedPlugin, highestCompatibleVersion));
            }
        }
        return upgradeSuggestions;
    }

    public Collection<PluginCompatibility> getIncompatibilePlugins()
    {
        return getIncompatiblePlugins(targetApplication);
    }

    public Collection<PluginCompatibility> getIncompatiblePlugins(TargetApplication targetApplication)
    {
        return wrap(filter(notNullAndNotCompatible(targetApplication)));
    }

    public Collection<Plugin> getCompatiblePlugins()
    {
        return getCompatiblePlugins(targetApplication);
    }

    public Collection<Plugin> getCompatiblePlugins(TargetApplication targetApplication)
    {
        return filter(notNullAndCompatible(targetApplication)).keySet();
    }

    public Collection<Plugin> getUnknownPlugins()
    {
        return filter(Predicates.<ApplicationCompatibility>isNull()).keySet();
    }

    private Collection<PluginCompatibility> wrap(Map<Plugin, ApplicationCompatibility> map)
    {
        Collection<PluginCompatibility> list = newArrayList();
        for (Map.Entry<Plugin, ApplicationCompatibility> entry : map.entrySet())
        {
            final Plugin installedPlugin = entry.getKey();
            final ApplicationCompatibility applicationCompatibility = entry.getValue();
            final MarketplacePluginVersion highestCompatibleVersion = highestCompatibleVersions.get(installedPlugin);
            final MarketplacePlugin marketplacePlugin = plugins.get(installedPlugin);

            list.add(new PluginCompatibility(installedPlugin, applicationCompatibility, highestCompatibleVersion, marketplacePlugin));
        }
        return list;
    }

    private Map<Plugin, ApplicationCompatibility> filter(Predicate<ApplicationCompatibility> compatibilityPredicate)
    {
        return filterValues(pluginCompatibilities, compatibilityPredicate);
    }

    public ServerInstance getServerInstance()
    {
        return serverInstance;
    }

    public TargetApplication getTargetApplication()
    {
        return targetApplication;
    }

    private Predicate<ApplicationCompatibility> notNullAndNotCompatible(TargetApplication targetApplication)
    {
        return and(not(isNull()), not(isCompatible(targetApplication)));
    }

    private Predicate<ApplicationCompatibility> notNullAndCompatible(TargetApplication targetApplication)
    {
        return and(not(isNull()), isCompatible(targetApplication));
    }

    private Predicate<ApplicationCompatibility> isCompatible(final TargetApplication targetApplication)
    {
        return new Predicate<ApplicationCompatibility>()
        {
            @Override
            public boolean apply(ApplicationCompatibility compatibility)
            {
                final boolean buildNumberTooLow = targetApplication.getBuildNumber() < compatibility.getMin()
                                                                                                    .getBuildNumber();
                final boolean buildNumberTooHigh = targetApplication.getBuildNumber() > compatibility.getMax()
                                                                                                     .getBuildNumber();

                return !(buildNumberTooHigh || buildNumberTooLow);
            }
        };
    }

    private Map<Plugin, ApplicationCompatibility> getCompatibilityMap(TargetApplication targetApplication)
    {
        final Map<Plugin, ApplicationCompatibility> results = Maps.newLinkedHashMap();
        for (Plugin installedPlugin : plugins.keySet())
        {
            final MarketplacePlugin marketplacePlugin = plugins.get(installedPlugin);
            if (marketplacePlugin != null)
            {
                final ApplicationCompatibility applicationCompatibility = marketplacePlugin.getApplicationCompatibility(installedPlugin, targetApplication);
                results.put(installedPlugin, applicationCompatibility);
            }
            else
            {
                results.put(installedPlugin, null);
            }
        }
        return results;

    }

    public static class Builder
    {
        private final TargetApplication targetApplication;
        private final ServerInstance serverInstance;
        private final Map<Plugin, Future<MarketplacePlugin>> compatibilityFutures = newLinkedHashMap();

        public Builder(TargetApplication targetApplication, ServerInstance serverInstance)
        {
            this.targetApplication = targetApplication;
            this.serverInstance = serverInstance;
        }

        public void add(Plugin installedPlugin, Future<MarketplacePlugin> compatibilityHolder)
        {
            compatibilityFutures.put(installedPlugin, compatibilityHolder);
        }

        private Map<Plugin, MarketplacePlugin> resolve() throws ExecutionException, InterruptedException
        {
            final Map<Plugin, MarketplacePlugin> results = newLinkedHashMap();
            for (Plugin installedPlugin : compatibilityFutures.keySet())
            {
                final Future<MarketplacePlugin> pluginFuture = compatibilityFutures.get(installedPlugin);
                results.put(installedPlugin, pluginFuture.get());
            }
            return results;
        }

        public PluginCompatibilityReport build() throws ExecutionException, InterruptedException
        {
            return new PluginCompatibilityReport(targetApplication, serverInstance, resolve());
        }
    }
}
