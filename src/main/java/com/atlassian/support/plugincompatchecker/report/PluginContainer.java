package com.atlassian.support.plugincompatchecker.report;

import java.util.Collection;

public interface PluginContainer
{
    Collection<? extends Plugin> getPlugins();
}
