package com.atlassian.support.plugincompatchecker.report;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface PluginCompatibilityReportFactory
{
    PluginCompatibilityReport getPluginCompatibilityReport(TargetApplication targetApplication, ServerInstance serverInstance, PluginContainer pluginContainer) throws IOException, ExecutionException, InterruptedException;
}
