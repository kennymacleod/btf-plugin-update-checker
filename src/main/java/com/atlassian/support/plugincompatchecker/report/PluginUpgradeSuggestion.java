package com.atlassian.support.plugincompatchecker.report;

import com.atlassian.support.plugincompatchecker.marketplace.MarketplacePluginVersion;

public class PluginUpgradeSuggestion
{
    private final Plugin installedPlugin;
    private final MarketplacePluginVersion suggestedVersion;

    public PluginUpgradeSuggestion(Plugin installedPlugin, MarketplacePluginVersion suggestedVersion)
    {
        this.installedPlugin = installedPlugin;
        this.suggestedVersion = suggestedVersion;
    }

    public Plugin getInstalledPlugin()
    {
        return installedPlugin;
    }

    public MarketplacePluginVersion getSuggestedVersion()
    {
        return suggestedVersion;
    }

}
