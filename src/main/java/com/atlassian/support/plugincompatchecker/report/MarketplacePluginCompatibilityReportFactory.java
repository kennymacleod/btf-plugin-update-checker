package com.atlassian.support.plugincompatchecker.report;

import com.atlassian.support.plugincompatchecker.marketplace.MarketplacePlugin;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import static java.lang.String.format;

public class MarketplacePluginCompatibilityReportFactory implements PluginCompatibilityReportFactory
{
    private static final Logger log = LoggerFactory.getLogger(MarketplacePluginCompatibilityReportFactory.class);
    private final ExecutorService marketplaceRequestExecutor;

    public MarketplacePluginCompatibilityReportFactory(ExecutorService marketplaceRequestExecutor)
    {
        this.marketplaceRequestExecutor = marketplaceRequestExecutor;
    }

    @Override
    public PluginCompatibilityReport getPluginCompatibilityReport(TargetApplication targetApplication, ServerInstance serverInstance, PluginContainer pluginContainer) throws IOException, ExecutionException, InterruptedException
    {
        final PluginCompatibilityReport.Builder reportBuilder = new PluginCompatibilityReport.Builder(targetApplication, serverInstance);
        for (final Plugin installedPlugin : pluginContainer.getPlugins())
        {
            Callable<MarketplacePlugin> call = new Callable<MarketplacePlugin>()
            {
                @Override
                public MarketplacePlugin call() throws Exception
                {
                    return getMarketplaceVersion(installedPlugin);
                }
            };
            reportBuilder.add(installedPlugin, marketplaceRequestExecutor.submit(call));
        }
        return reportBuilder.build();
    }

    private MarketplacePlugin getMarketplaceVersion(Plugin installedPlugin) throws IOException
    {
        String url = format("https://marketplace.atlassian.com/rest/1.0/plugins/%s", installedPlugin.getPluginKey());
        HttpClient httpClient = new HttpClient();
        final GetMethod get = new GetMethod(url);

        try
        {
            log.info("Querying Marketplace for " + installedPlugin.getPluginKey());
            final int statusCode = httpClient.executeMethod(get);

            if (200 == statusCode)
            {
                return new ObjectMapper().readValue(get.getResponseBodyAsStream(), MarketplacePlugin.class);
            }
            return null;
        }
        finally
        {
            get.releaseConnection();
        }
    }
}
